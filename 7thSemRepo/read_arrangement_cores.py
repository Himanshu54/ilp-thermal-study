#! /usr/bin/python

"""
input pattern of core 
[
    [0,1,2,3],
    [4,5,6,7]
]

output npr matrix
[
    0[0,1,0,0,1,1,0,0],
    1[1,0,1,0,1,1,1,0],
    2[0,1,0,1,0,1,1,1],
    3[0,0,1,0,0,0,1,1],
    4[1,1,0,0,0,1,0,0],
    5[1,1,1,0,1,0,1,0],
    6[0,1,1,1,0,1,0,1],
    7[0,0,1,1,0,0,1,0],
]
"""
def get_adj_node ( core_arr_pattern, idx_i,idx_j ):
    lim_i = len(core_arr_pattern)
    lim_j = len(core_arr_pattern[0])
    
    ret = []
    if(idx_i -1 >= 0 ):
        if( idx_j-1 >= 0 and core_arr_pattern[idx_i-1][idx_j-1]!='$'):
            ret.append(core_arr_pattern[idx_i-1][idx_j-1])
        if( idx_j+1 < lim_j and core_arr_pattern[idx_i-1][idx_j+1]!='$'):
            ret.append(core_arr_pattern[idx_i-1][idx_j+1])
        if( core_arr_pattern[idx_i-1][idx_j] != '$'):
            ret.append(core_arr_pattern[idx_i-1][idx_j])
        
    if(idx_i +1 < lim_i ):
        if( idx_j-1 >= 0 and core_arr_pattern[idx_i+1][idx_j-1]!='$'):
            ret.append(core_arr_pattern[idx_i+1][idx_j-1])
        if( idx_j+1 < lim_j and core_arr_pattern[idx_i+1][idx_j+1]!='$'):
            ret.append(core_arr_pattern[idx_i+1][idx_j+1])
        if( core_arr_pattern[idx_i+1][idx_j] != '$'):
            ret.append(core_arr_pattern[idx_i+1][idx_j])

    if(idx_j-1 >= 0 and core_arr_pattern[idx_i][idx_j-1] != '$' ):
        ret.append(core_arr_pattern[idx_i][idx_j-1])

    if(idx_j+1 < lim_j and core_arr_pattern[idx_i][idx_j+1] != '$'):
        ret.append(core_arr_pattern[idx_i][idx_j+1])

    return ret

def pattern_to_npr( core_arr_pattern ):
    """
    finding max core index
    """
    max_core_idx = 0
    for row in core_arr_pattern:
        for col in row:
            if ( col != '$' and max_core_idx < col ):
                max_core_idx = col
    w  = max_core_idx + 1 
    Matrix = [[0 for x in range(w)] for y in range(w)] 
    
    idx_i = 0 
    idx_j = 0
    for row in core_arr_pattern:
        idx_j = 0
        for col in row:
            if( col != '$'):
                adj_list = get_adj_node(core_arr_pattern,idx_i,idx_j)
                for x in adj_list:
                    Matrix[col][x] = 1
            idx_j = idx_j+1
        idx_i = idx_i+1

    return Matrix
import pprint