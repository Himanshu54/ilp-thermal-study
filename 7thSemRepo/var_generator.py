#!/usr/bin/env python3
"""Generate various variable names and types"""

# x_i_p
def get_processor_allocation_vars(no_of_tasks, no_of_processors):
    return (generate_2d_var_names('x', no_of_tasks, no_of_processors),
            generate_2d_var_types('B', no_of_tasks, no_of_processors))

# s_i
def get_start_time_vars(no_of_tasks):
    return (generate_1d_var_names('start', no_of_tasks),
            generate_1d_var_types('I', no_of_tasks))

# tau_i
def get_end_time_vars(no_of_tasks):
    return (generate_1d_var_names('end', no_of_tasks),
            generate_1d_var_types('I', no_of_tasks))

# t_i
def get_execution_time_vars(no_of_tasks):
    return (generate_1d_var_names('exec', no_of_tasks),
            generate_1d_var_types('I', no_of_tasks))

# n_p_r
def get_adjacent_cores_vars(no_of_cores):
    names = generate_2d_var_names('adj', no_of_cores, no_of_cores)
    types = generate_2d_var_types('B', no_of_cores, no_of_cores)
    names = remove_diagonal_var_names(names)
    types = types[:len(names)]
    return names, types

# q_i_k
def get_above_temp_vars(no_of_tasks, no_of_speeds):
    return (generate_2d_var_names('above_temp', no_of_tasks, no_of_speeds),
            generate_2d_var_types('I', no_of_tasks, no_of_speeds))

# y_i_k
def get_exec_speed_vars(no_of_tasks, no_of_speeds):
    return (generate_2d_var_names('exec_speed', no_of_tasks, no_of_speeds),
            generate_2d_var_types('B', no_of_tasks, no_of_speeds))

# p_i_j
def get_starts_before_vars(no_of_tasks):
    names = generate_2d_var_names('starts_bef', no_of_tasks, no_of_tasks)
    types = generate_2d_var_types('B', no_of_tasks, no_of_tasks)
    names = remove_diagonal_var_names(names)
    types = types[:len(names)]
    return names, types


# d_i_j
def get_end_after_start_vars(no_of_tasks):
    names = generate_2d_var_names('end_after_start', no_of_tasks, no_of_tasks)
    types = generate_2d_var_types('B', no_of_tasks, no_of_tasks)
    names = remove_diagonal_var_names(names)
    types = types[:len(names)]
    return names, types


def remove_diagonal_var_names(var_list):
    return [var for var in var_list if not is_diagonal_var(var)]


def generate_1d_var_names(prefix, no_of_vars):
    names = []
    for i in range(no_of_vars):
        names.append(f'{prefix}_{i}')
    return names


def generate_2d_var_names(prefix, no_of_rows, no_of_cols):
    names = []
    for i in range(no_of_rows):
        for j in range(no_of_cols):
            names.append(f'{prefix}_{i}_{j}')
    return names


def generate_1d_var_types(type, no_of_vars):
    return generate_2d_var_types(type, no_of_vars, 1)


def generate_2d_var_types(type, no_of_rows, no_of_cols):
    return type * (no_of_rows * no_of_cols)


def is_diagonal_var(var):
    parts = var.split('_')[1:]
    if parts[0] == parts[1]:
        return True
    return False

