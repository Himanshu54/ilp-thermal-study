from __future__ import print_function
from docplex.mp.model import Model
from sys import stdout
from reader import *
from read_arrangement_cores import *

from docplex.cp.config import context
context.solver.agent = 'local'
context.solver.local.execfile = '/home/hemuhemu/Desktop/pro7/project/cpoptimizer/bin/x86-64_linux/cpoptimizer'

#-----------------------------------------------------------------------------
# Initialize the problem data
#-----------------------------------------------------------------------------

#filename  : name of task graph file

filename = 'task.graph'
print ("reading task graph from file "+filename)
# Number of processor
NB_PCR = number_of_processors(filename)
print ("number of processors = ",NB_PCR)
# Number of tasks
NB_TSK = nodes_in_task_graph(filename)
print ("number of task nodes = ",NB_TSK)
# Voltage Levels
NB_VL  = 3
print ("number of voltage levels = ",NB_VL)

# # q_i_k time spent above thresh hold temperature by Task[i] while running at Voltage[k]
# q = [[0 for i in range(NB_VL)] for j in range(NB_TSK)]
# WCET for task i at voltage level k
wcet = time_i_k(filename)


#p[i,j] = 1 if Task[i] start before Task[j] 
P = task_precedene_gen(filename,NB_TSK)



#N[p,r] = 1 if P_p is adjacent to P_r

core_arr_pattern = [
                        [0,1,2,3],
                        [4,5,6,7]
                    ]
n = pattern_to_npr( core_arr_pattern)
#-----------------------------------------------------------------------------
# Prepare the data for modeling
#-----------------------------------------------------------------------------
task_range = range(NB_TSK)
processor_range = range(NB_PCR)
voltage_levels = range(NB_VL)

# A
A =[[0 for k in voltage_levels]for p in processor_range]

# O 
O = [[0 for k in voltage_levels]for i in task_range]

# C communication cost
C = P


#-----------------------------------------------------------------------------
# Build the model
#-----------------------------------------------------------------------------

# Create model

mdl = Model()

# decision variables
# x[i,p] = 1 if task i is assigned to processor p else 0
x = mdl.binary_var_matrix(task_range,processor_range,lambda ij: "x_%d%d" %(ij[0],ij[1]))

#y[i,k] = 1  if task i runs at voltage level k else 0
y = mdl.binary_var_matrix(task_range,voltage_levels,lambda ij: "y_%d%d" %(ij[0],ij[1]))


# Start Time for each task
st_time = mdl.integer_var_list(task_range, lb=None, ub=None ,name ="st_time")

# Finish time for each task
fn_time = mdl.integer_var_list(task_range, lb=None, ub=None ,name ="fn_time")

#--------------------------------------------------------)-------------------------------------
# Constraints
#---------------------------------------------------------------------------------------------

#===(1)=== each task is assigned to only 1 PU
mdl.add_constraints(mdl.sum(x[i,p]for p in processor_range) == 1 for i in task_range)

#===(2)=== each task run at only one V/f level
mdl.add_constraints(mdl.sum(y[i,k]for k in voltage_levels) == 1 for i in task_range)

#===(3)=== 
for i in task_range:
    for k in voltage_levels:
        for p in processor_range:
            mdl.add_constraint(y[i,k] <= A[p][k]*x[i,p])

#===(4)===
for i in task_range:
    li = [st_time[i]]
    for k in voltage_levels:
        li.append(y[i,k]*O[i][k])
    mdl.add_constraint(mdl.sum(li) == fn_time[i])

#===(5)===
for i in task_range:
    for j in task_range:
        if(P[i][j]):
            mdl.add_constraints((fn_time[i]+ C[i][j]*(x[i,p]-x[j,p]) <= st_time[j])for p in processor_range)



# Objective
mdl.minimize( mdl.sum(x[i,p] * y[i,k] for k in voltage_levels for i in task_range for p in processor_range))


# Solve model
print("Solving model....")
msol = mdl.solve(TimeLimit=50)

# Print solution
if msol:
    print("Number of moves:")
    for i in processor_range:
        for j in processor_range:
            print(x[i,j],end=' = ')
            print(x[i,j].solution_value,end='\t')
        print("")
else:
    print ("Solve status: " + msol.get_solve_status() + ", fail status: " + msol.get_fail_status() + "\n")
