import re

def read_file_into_lines(filename):    
    fp = open(filename,'r')
    lines = list(fp)
    lines = [(x.replace("\n",""))for x in lines]
    fp.close()
    return lines

def nodes_in_task_graph(filename):
    lines = read_file_into_lines(filename)
    temp = int(filter(lambda x:re.search(r't_nst',x),lines)[0].split()[1])
    t_nst = temp
    return t_nst

def number_of_processors(filename):
    lines = read_file_into_lines(filename)
    temp = int(filter(lambda x:re.search(r'No_of_Processor',x),lines)[0].split()[1])
    nb_pu = temp
    return nb_pu
    

def time_i_k(filename):
    lines = read_file_into_lines(filename)
    temp = filter(lambda x:re.search(r'st[0-9]+',x),lines)
    temp = [ x.split()[1:] for x in temp]
    temp = [map(int,i) for i in temp]
    t_i_k = temp
    return t_i_k

def task_precedene_gen(filename,t_nst):
    lines = read_file_into_lines(filename)
    task_prec = [[0 for x in range(t_nst)] for y in range(t_nst)]
    temp = filter(lambda x:re.search(r'Link_from',x),lines)
    temp = [ x.split()[1:] for x in temp]
    temp = [map(int,i) for i in temp]
    for x in temp:
        task_prec[x[0]][x[1]] = x[2]
    return task_prec    


