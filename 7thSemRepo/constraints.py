#4th constraint
start=[]
end=[]
n
m
mat[][]
store_start=[]
store_end=[]
store_time=[]
for i in range(n):
	for j in range(m):
		if(mat[i][j]>0)
			store_start.append(start[j])
			store_end.append(end[i])
			store_time.append(mat[i][j])

for i in range(len(store_start)):
	thevars=[]
	thevars.append(store_start[i])
	thevars.append(store_end[i])
	thecoeffs=[1.0,-1.0]
	c.linear_constraints.add(lin_expr=[cplex.SparsePair(thevars,thecoeffs)],senses=['G'],rhs=[store_time[i]-1])

#3rd constraint(needs to be checked)
start=[]
end=[]
execution_time=[]

for i in range(len(start)):
	thevars=[]
	thevars.append(end[i])
	thevars.append(start[i])
	thecoeffs=[1.0,-1.0]
	c.linear_constraints.add(lin_expr=[cplex.SparsePair(thevars,thecoeffs)],senses=['E'],rhs=[execution_time[i]])


#first constraint
x
no_of_task
no_of_processors
task_temp=[]
for i in range(no_of_task):
	for j in (no_of_processors):
		task_temp.append(x[i][j])
c.linear_constraints.add(lin_expr=[cplex.SparsePair(task_temp,[1]*len(task_temp))],senses=["E"],rhs=[1])

#second constraint
y
no_of_task
no_of_voltage_levels
vol_temp=[]
for i in range(no_of_task):
	for k in range(no_of_voltage_levels):
		vol_temp.append(y[i][k])
c.linear_constraints.add(lin_expr=[cplex.SparsePair(vol_temp,[1]*len(vol_temp))],senses=["E"],rhs=[1])

#5th constraint
task_deadline=[]
no_of_task
end=[]
for i in range(no_of_task):
	c.linear_constraints.add(lin_expr=[cplex.SparsePair(task_deadline[i],[1]),senses=["G"],rhs=[end[i]-1]])

#6th constraint
start=[]
end=[]
p[][]
no_of_task
store_start=[]
store_end=[]
store_time=[]
for i in range(no_of_task):
	for j in range(no_of_task):
		if(p[j][i]==1):
			store_start.append(start[i])
			store_end.append(end[j])

for i in range(len(store_start)):
	thevars=[]
	thevars.append(store_start[i])
	thevars.append(store_end[i])
	thecoeffs=[1.0,-1.0]
	c.linear_constraints.add(lin_expr=[cplex.SparsePair(thevars,thecoeffs)],senses=['G'],rhs=[-1])

#7th constraint
x[][]
p[][]
no_of_task
no_of_processor
temp_1=[]
temp_2=[]
for i in range(no_of_task):
	for j in range(no_of_task):
		for p in range(no_of_processor):
			if(x[i][p]==x[j][p] and x[i][p]==1):
				 temp_1.append(p[i][j])
				 temp_2.append(p[j][i])

for i in range(len(temp_1)):
	thevars=[]
	thevars.append(temp_1[i])
	thevars.append(temp_2[i])
	thecoeffs=[1.0,1.0]
	c.linear_constraints.add(lin_expr=[cplex.SparsePair(thevars,thecoeffs)],senses=["E"],rhs=[1])

