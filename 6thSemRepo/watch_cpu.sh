#!/bin/bash
# Usage instructions
# For frequency ./watch_cpu.sh freq
# For temperature ./watch_cpu.sh temp

if [ "$1" = "freq" ]; then
        watch -n 1 tail -vn 1 /sys/devices/system/cpu/cpu{0..7}/cpufreq/scaling_cur_freq
elif [ "$1" = "temp" ]; then
        watch -n 1 tail -vn 1 /sys/devices/platform/coretemp.0/hwmon/hwmon1/temp*_input
else
        echo "No arguments provided. Read comments for usage."
fi
