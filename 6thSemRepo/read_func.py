#!/usr/bin/env python3
"""Helper function to read from file"""

import linecache


def read_log(file, ln):
    line = linecache.getline(file, lineno=ln).rstrip()
    line = line.split(sep=';')
    core_data = []
    for freq_temp in line:
        if freq_temp.__contains__(','):
            core_data.append([int(freq_temp.split(',')[0][:2]), int(freq_temp.split(',')[1])])
        else:
            package_temp = int(freq_temp[:2])
    return core_data, package_temp

# print(read_log('Data/memory_test_suite.log', 1))
