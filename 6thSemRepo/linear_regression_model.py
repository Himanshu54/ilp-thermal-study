#!/usr/bin/env python3
"""Implement linear regression model to predict temperature"""

from numpy import array
from sklearn import linear_model
from read_func import read_log
import numpy as np
from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_error
import math
import matplotlib.pyplot as plt


def read_file(file, lc):
    X = []
    y = []
    for i in range(lc):
        core_d, package_temp = read_log(file, i + 1)
        X.append([elem[1] for elem in core_d])
        X[i].append(package_temp)
        y.append([elem[0] for elem in core_d])

    return X, y



def main():
    train_file = 'Data/all_data' #input('Enter file to read from: ')
    test_file = 'Data/memory_test_suite.log'

    train_lc = sum([1 for _ in open(train_file)])
    test_lc = sum([1 for _ in open(test_file)])

    X_train, Y_train = read_file(train_file, train_lc)
    X_test, Y_test = read_file(test_file, test_lc)
    X_train = array(X_train)
    Y_train = array(Y_train)
    X_test = array(X_test)
    Y_test = array(Y_test)

    model = linear_model.LinearRegression()
    model.fit(X_train, Y_train)
    Y_predicted = model.predict(X_test)

    with open('Result/memory', 'w') as f:
        for core in range(Y_predicted.shape[1]):
            ans = mean_absolute_error(Y_test[:,core], Y_predicted[:,core])
            print(core, '\t', ans, file=f)


    for core in range(Y_predicted.shape[1]):
        # plt.scatter(Y_test[:,core], Y_predicted[:,core])
        # plt.xlabel('Actual Core Temperature in Celsius')
        # plt.ylabel('Predicted Core Temperature in Celsius')
        # plt.title('Core' + str(core))
        # plt.show()

        time = np.array([i for i in range(Y_test.shape[0])]).T
        plt.title('Core '+str(core))
        plt.scatter(time, Y_test[:, core], color='blue', marker='.', label='Actual')
        plt.scatter(time, Y_predicted[:, core], color='red', marker='*', label='Predicted')
        plt.legend()
        plt.ylabel("Temperature (C)")
        plt.xlabel("Time elapsed (s)")
        # plt.show()
        plt.savefig('Result/memoryCore ' + str(core) + '.png')
        plt.clf()


if __name__ == '__main__':
    main()
