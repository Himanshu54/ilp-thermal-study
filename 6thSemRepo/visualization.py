#!/usr/bin/env python3
"""Plotting collected data"""

##########################
#  *********
#  * 0 * 1 *
#  *********
#  * 2 * 3 *
#  *********
#  * 4 * 5 *
#  *********
#  * 6 * 7 *
#  *********
##########################

import matplotlib.pyplot as plt
import read_func


def plot_data(file, lc):
    fig, (ax0, ax1) = plt.subplots(nrows=2)
    ax0.set_axis_off()
    ax1.set_axis_off()

    for i in range(lc):
        core_d, package_temp = read_func.read_log(file, i+1)

        temp_d = [[core_d[6][0], core_d[7][0]],
                  [core_d[4][0], core_d[5][0]],
                  [core_d[2][0], core_d[3][0]],
                  [core_d[0][0], core_d[1][0]]]
        temp_cmap = plt.get_cmap('plasma')

        temp_plot = ax0.pcolormesh(temp_d, cmap=temp_cmap, vmin=20, vmax=80, edgecolor='black')
        if i == 0:
            fig.colorbar(temp_plot, ax=ax0)

        freq_d = [[core_d[6][1], core_d[7][1]],
                  [core_d[4][1], core_d[5][1]],
                  [core_d[2][1], core_d[3][1]],
                  [core_d[0][1], core_d[1][1]]]
        freq_cmap = plt.get_cmap('viridis')

        freq_plot = ax1.pcolormesh(freq_d, cmap=freq_cmap, vmin=1200000, vmax=2400000, edgecolor='black')
        if i == 0:
            fig.colorbar(freq_plot, ax=ax1)

        fig.tight_layout()
        # plt.show()
        plt.pause(0.001)


def main():
    file = input('Enter file to read from: ')
    line_cnt = sum(1 for _ in open(file))
    plot_data(file, line_cnt)


if __name__ == '__main__':
    main()
