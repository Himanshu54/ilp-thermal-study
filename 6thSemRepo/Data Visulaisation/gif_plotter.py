import matplotlib
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib.patches as patches


font = {'family': 'serif',
        'color':  'black',
        'weight': 'normal',
        'size': 15,
        }

fig = plt.figure()
ax = fig.add_subplot(111)


fig.canvas.set_window_title('Frequency voltage data visualisation')
offset_x = 0
offset_y = 0
time = 0

#taking the input from the file(temp_freq.log) and storing it into 2D list temp and freq

file = open("../Data/aobench.log","r")
temp = []
freq = []
tempchip = []

for line in file:
    fields = line.split(";")
    temp1 = []
    freq1 = []
    for x in fields:
        subfields = x.split(",")
        if len(subfields) == 1:
            tempchip.append(subfields[0][:len(subfields[0])-4]) #only taking the first two digit of temperature
        else :
            temp1.append(subfields[0][:len(subfields[0])-3])
            freq1.append(subfields[1])
    temp.append(temp1)
    freq.append(freq1)

#function for finding the ratio of light intensity

def intensity(minimum, maximum, value):
    value = value 
    minimum, maximum = float(minimum), float(maximum)
    ratio = (value-minimum) / (maximum - minimum)
    return ratio

def animate(i):    
    ax.clear()  
    global time
    plt.axis('off') #hiding the axis
    fig.suptitle('Effects at time instance t ='+str(time),fontsize=20)
    ax.set_xlim(0,2.2)	
    #adding patches and text to the output plot

    ax.add_patch(patches.Rectangle((offset_x+0.0,offset_y+0.0),0.2,0.2,linewidth=3,fill=True,facecolor='#ff0000',alpha=intensity(20,80,int(temp[time][6]))))
    ax.add_patch(patches.Rectangle((offset_x+0.2,offset_y+0.0),0.2,0.2,linewidth=3,fill=True,facecolor='#ff0000',alpha=intensity(20,80,int(temp[time][7]))))
    ax.add_patch(patches.Rectangle((offset_x+0.0,offset_y+0.2),0.2,0.2,linewidth=3,fill=True,facecolor='#ff0000',alpha=intensity(20,80,int(temp[time][4]))))
    ax.add_patch(patches.Rectangle((offset_x+0.2,offset_y+0.2),0.2,0.2,linewidth=3,fill=True,facecolor='#ff0000',alpha=intensity(20,80,int(temp[time][5]))))
    ax.add_patch(patches.Rectangle((offset_x+0.0,offset_y+0.4),0.2,0.2,linewidth=3,fill=True,facecolor='#ff0000',alpha=intensity(20,80,int(temp[time][2]))))
    ax.add_patch(patches.Rectangle((offset_x+0.2,offset_y+0.4),0.2,0.2,linewidth=3,fill=True,facecolor='#ff0000',alpha=intensity(20,80,int(temp[time][3]))))
    ax.add_patch(patches.Rectangle((offset_x+0.0,offset_y+0.6),0.2,0.2,linewidth=3,fill=True,facecolor='#ff0000',alpha=intensity(20,80,int(temp[time][0]))))
    ax.add_patch(patches.Rectangle((offset_x+0.2,offset_y+0.6),0.2,0.2,linewidth=3,fill=True,facecolor='#ff0000',alpha=intensity(20,80,int(temp[time][1]))))

    ax.add_patch(patches.Rectangle((offset_x+0.6,offset_y+0.0),0.2,0.2,linewidth=3,fill=True,facecolor='#006B96',alpha=intensity(1000000,2400000,int(freq[time][6]))))
    ax.add_patch(patches.Rectangle((offset_x+0.8,offset_y+0.0),0.2,0.2,linewidth=3,fill=True,facecolor='#006B96',alpha=intensity(1000000,2400000,int(freq[time][7]))))
    ax.add_patch(patches.Rectangle((offset_x+0.6,offset_y+0.2),0.2,0.2,linewidth=3,fill=True,facecolor='#006B96',alpha=intensity(1000000,2400000,int(freq[time][4]))))
    ax.add_patch(patches.Rectangle((offset_x+0.8,offset_y+0.2),0.2,0.2,linewidth=3,fill=True,facecolor='#006B96',alpha=intensity(1000000,2400000,int(freq[time][5]))))
    ax.add_patch(patches.Rectangle((offset_x+0.6,offset_y+0.4),0.2,0.2,linewidth=3,fill=True,facecolor='#006B96',alpha=intensity(1000000,2400000,int(freq[time][2]))))
    ax.add_patch(patches.Rectangle((offset_x+0.8,offset_y+0.4),0.2,0.2,linewidth=3,fill=True,facecolor='#006B96',alpha=intensity(1000000,2400000,int(freq[time][3]))))
    ax.add_patch(patches.Rectangle((offset_x+0.6,offset_y+0.6),0.2,0.2,linewidth=3,fill=True,facecolor='#006B96',alpha=intensity(1000000,2400000,int(freq[time][0]))))
    ax.add_patch(patches.Rectangle((offset_x+0.8,offset_y+0.6),0.2,0.2,linewidth=3,fill=True,facecolor='#006B96',alpha=intensity(1000000,2400000,int(freq[time][1]))))

    ax.text(offset_x+0.060, offset_y+0.070, '6', fontsize=15)
    ax.text(offset_x+0.260, offset_y+0.070, '7', fontsize=15)
    ax.text(offset_x+0.060, offset_y+0.270, '4', fontsize=15)
    ax.text(offset_x+0.260, offset_y+0.270, '5', fontsize=15)
    ax.text(offset_x+0.060, offset_y+0.470, '2', fontsize=15)
    ax.text(offset_x+0.260, offset_y+0.470, '3', fontsize=15)
    ax.text(offset_x+0.060, offset_y+0.670, '0', fontsize=15)
    ax.text(offset_x+0.260, offset_y+0.670, '1', fontsize=15)

    ax.text(offset_x+0.660, offset_y+0.070, '6', fontsize=15)
    ax.text(offset_x+0.860, offset_y+0.070, '7', fontsize=15)
    ax.text(offset_x+0.660, offset_y+0.270, '4', fontsize=15)
    ax.text(offset_x+0.860, offset_y+0.270, '5', fontsize=15)
    ax.text(offset_x+0.660, offset_y+0.470, '2', fontsize=15)
    ax.text(offset_x+0.860, offset_y+0.470, '3', fontsize=15)
    ax.text(offset_x+0.660, offset_y+0.670, '0', fontsize=15)
    ax.text(offset_x+0.860, offset_y+0.670, '1', fontsize=15)

    ax.text(1.15, 0.50, r'$temperture\ of\ the\ chip:\ $'+tempchip[time]+r'$^\circ$', fontdict=font)
    ax.text(1.15, 0.45, r'$temperature$',fontsize=15)

    ax.text(1.15, 0.35, r'$core\ 0\  :$'+temp[time][0]+r'$^\circ$', fontdict=font)
    ax.text(1.15, 0.30, r'$core\ 1\  :$'+temp[time][1]+r'$^\circ$', fontdict=font)
    ax.text(1.15, 0.25, r'$core\ 2\  :$'+temp[time][2]+r'$^\circ$', fontdict=font)
    ax.text(1.15, 0.20, r'$core\ 3\  :$'+temp[time][3]+r'$^\circ$', fontdict=font)
    ax.text(1.15, 0.15, r'$core\ 4\  :$'+temp[time][4]+r'$^\circ$', fontdict=font)
    ax.text(1.15, 0.10, r'$core\ 5\  :$'+temp[time][5]+r'$^\circ$', fontdict=font)
    ax.text(1.15, 0.05, r'$core\ 6\  :$'+temp[time][6]+r'$^\circ$', fontdict=font)
    ax.text(1.15, 0.00, r'$core\ 7\  :$'+temp[time][7]+r'$^\circ$', fontdict=font)

    ax.text(1.8, 0.45, r'$frequency$',fontsize=15)

    ax.text(1.8, 0.35,freq[time][0], fontdict=font)
    ax.text(1.8, 0.30,freq[time][1], fontdict=font)
    ax.text(1.8, 0.25,freq[time][2], fontdict=font)
    ax.text(1.8, 0.20,freq[time][3], fontdict=font)
    ax.text(1.8, 0.15,freq[time][4], fontdict=font)
    ax.text(1.8, 0.10,freq[time][5], fontdict=font)
    ax.text(1.8, 0.05,freq[time][6], fontdict=font)
    ax.text(1.8, 0.00,freq[time][7], fontdict=font)

    time = time + 1
ani = animation.FuncAnimation(fig, animate, interval=100)
#plt.tight_layout()
plt.show()
#ani.save('animation.gif', writer='imagemagick',fps=10)
