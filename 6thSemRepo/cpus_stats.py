#!/usr/bin/env python3
"""Log temperature and frequency data"""

import time


def main():
    temp_prefix = '/sys/devices/platform/coretemp.0/hwmon/hwmon1'
    freq_prefix = '/sys/devices/system/cpu/'
    freq_suffix = '/cpufreq/scaling_cur_freq'

    package_temp = open(temp_prefix + '/temp1_input', 'r')
    core0_temp = open(temp_prefix + '/temp2_input', 'r')
    core1_temp = open(temp_prefix + '/temp3_input', 'r')
    core2_temp = open(temp_prefix + '/temp4_input', 'r')
    core3_temp = open(temp_prefix + '/temp5_input', 'r')
    core4_temp = open(temp_prefix + '/temp6_input', 'r')
    core5_temp = open(temp_prefix + '/temp7_input', 'r')
    core6_temp = open(temp_prefix + '/temp8_input', 'r')
    core7_temp = open(temp_prefix + '/temp9_input', 'r')

    core0_freq = open(freq_prefix + 'cpu0' + freq_suffix, 'r')
    core1_freq = open(freq_prefix + 'cpu1' + freq_suffix, 'r')
    core2_freq = open(freq_prefix + 'cpu2' + freq_suffix, 'r')
    core3_freq = open(freq_prefix + 'cpu3' + freq_suffix, 'r')
    core4_freq = open(freq_prefix + 'cpu4' + freq_suffix, 'r')
    core5_freq = open(freq_prefix + 'cpu5' + freq_suffix, 'r')
    core6_freq = open(freq_prefix + 'cpu6' + freq_suffix, 'r')
    core7_freq = open(freq_prefix + 'cpu7' + freq_suffix, 'r')

    file_name = input('Enter file name: ')
    with open('./Data/'+file_name, 'w') as output:
        while True:
            # Log temperature and frequency data
            # format
            # <core0_temp>,<core0_freq>;<core1_temp>,<core1_freq>;...;<core7_temp>,<core7_freq>;<package_temp>
            print(core0_temp.read().rstrip(), core0_freq.read().rstrip(), sep=',', end=';', file=output, flush=True)
            print(core1_temp.read().rstrip(), core1_freq.read().rstrip(), sep=',', end=';', file=output, flush=True)
            print(core2_temp.read().rstrip(), core2_freq.read().rstrip(), sep=',', end=';', file=output, flush=True)
            print(core3_temp.read().rstrip(), core3_freq.read().rstrip(), sep=',', end=';', file=output, flush=True)
            print(core4_temp.read().rstrip(), core4_freq.read().rstrip(), sep=',', end=';', file=output, flush=True)
            print(core5_temp.read().rstrip(), core5_freq.read().rstrip(), sep=',', end=';', file=output, flush=True)
            print(core6_temp.read().rstrip(), core6_freq.read().rstrip(), sep=',', end=';', file=output, flush=True)
            print(core7_temp.read().rstrip(), core7_freq.read().rstrip(), sep=',', end=';', file=output, flush=True)
            print(package_temp.read().rstrip(), file=output, flush=True)

            # Resetting all pointers to the beginning
            package_temp.seek(0)
            core0_temp.seek(0)
            core1_temp.seek(0)
            core2_temp.seek(0)
            core3_temp.seek(0)
            core4_temp.seek(0)
            core5_temp.seek(0)
            core6_temp.seek(0)
            core7_temp.seek(0)
            core0_freq.seek(0)
            core1_freq.seek(0)
            core2_freq.seek(0)
            core3_freq.seek(0)
            core4_freq.seek(0)
            core5_freq.seek(0)
            core6_freq.seek(0)
            core7_freq.seek(0)
            time.sleep(1)

        
if __name__ == '__main__':
        main()
