from read_func import read_log
from matplotlib import pyplot as plt

lc = sum(1 for _ in open('Data/memory_test_suite.log', 'r'))
time = []
temp = []
freq = []
for i in range(lc):
    time.append(i)
    core_d, package_temp = read_log('Data/memory_test_suite.log', i+1)
    temp.append(core_d[0][0])
    freq.append(core_d[0][1])

new = []
sum = temp[0]
for i in range(len(temp)-1):
    new.append([temp[i]-sum, temp[i]])
    sum = temp[i]

plt.plot(time[:-1], new)
# fig, axes = plt.subplots(2, 1)
# axes[0].plot(time, temp)
# axes[1].plot(time[:-1], new)
plt.show()